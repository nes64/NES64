# NES64

The NES64 is a replacement PCB for use in an existing controller for the Nintendo
Entertainment System (NES). It brings one of the most popular and arguably best of the
early game console controllers to the Commodore 64, Amiga, Atari and other retro home computers.

------------------------

NES64 homepage: [nes64.pryds.eu](https://nes64.pryds.eu)

[Instructions](https://nes64.pryds.eu/instructions.html)

[Buy the PCB as a kit](https://autumnhippo.com/products/nes64)

